﻿/// <reference path="scripts/angular.js" />

var phoneApp = angular.module('phoneApp', []);
phoneApp.controller('PhoneListCtrl', function ($scope, $http) {
    $http.get('http://cdn.rawgit.com/angular/angular-phonecat/master/app/phones/phones.json').success(function (data) {
        $scope.phones = data;
    });

    $scope.orderProperty = 'age';
});